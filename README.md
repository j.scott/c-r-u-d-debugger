<h1>Create Retrieve Update and Delete Debugger</h1>
A simple HTML file to embed into a working project in order to test servlet end points.

Currently the format consists of a series of input fields on the left that are inserted into raw code on the right.
If a field is missing on the left, a developer can manually edit the code on the right.

Once the 'RUN' button is clicked the code on the right is retrieved and run in an iframe located at the bottom of the frame.